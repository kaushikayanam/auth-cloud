package ctxtypes

type ClientId string
type Vendor string

const CtxClientIdKey ClientId = "clientId"
const CtxVendorKey Vendor = "vendor"
