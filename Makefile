.PHONY: openapi
openapi: openapi_go

.PHONY: openapi_go
openapi_go:
	oapi-codegen -generate chi-server -o internal/pkg/interfaces/openapi/openapi_api_gen.go -package openapi api/auth.yml
	oapi-codegen -generate types -o internal/pkg/interfaces/openapi/openapi_types_gen.go -package openapi api/auth.yml

.PHONY: build
build:
	rm -rvf build
	mkdir build
	go build -o build/app internal/cmd/auth/main.go