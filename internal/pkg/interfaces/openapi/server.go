package openapi

import (
	"net/http"

	"nononsensecode.com/auth/internal/pkg/application"
	"nononsensecode.com/auth/internal/pkg/domain/model/user"
)

type MiddlewareProvider interface {
	GetMiddlewares() []func(http.Handler) http.Handler
}

type Server struct {
	userService *application.UserService
	middlewares []func(http.Handler) http.Handler
}

func (s *Server) GetMiddlewares() []func(http.Handler) http.Handler {
	return s.middlewares
}

func NewServer(userRepositoryProvider func() (user.Repository, error)) (s *Server, err error) {
	var userRepo user.Repository
	userRepo, err = userRepositoryProvider()
	if err != nil {
		return
	}

	userService := application.NewUserService(userRepo)

	s = &Server{
		userService: userService,
	}
	return
}
