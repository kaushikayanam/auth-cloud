package mysqldb

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"nononsensecode.com/auth/internal/pkg/domain/model/user"
	"nononsensecode.com/auth/internal/pkg/infrastructure/db"
)

type MySQLUserRepository struct{}

func (r MySQLUserRepository) Save(ctx context.Context, u user.User) (usr user.User, err error) {
	var d *sqlx.DB
	d, err = getMysqlConnection(ctx)
	if err != nil {
		return
	}

	tx, err := d.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		err = fmt.Errorf("unable to start transactions: %w", err)
		return
	}
	defer func() {
		err = db.FinishTransaction(tx, err)
	}()

	stmt, err := tx.PrepareContext(ctx, "INSERT INTO users (username, first_name, last_name, email, password) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		err = fmt.Errorf("statement cannot be created: %w", err)
		return
	}
	defer stmt.Close()

	// Using AutoIncrement id
	// If we need to check the result that can be done. Need to think
	result, err := stmt.ExecContext(ctx, u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	if err != nil {
		err = fmt.Errorf("statement cannot be executed: %w", err)
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		err = fmt.Errorf("id cannot be retrieved: %w", err)
		return
	}

	usr = user.UnmarshalFromPersistenceUnit(int32(id), u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	return
}

func getMysqlConnection(ctx context.Context) (d *sqlx.DB, err error) {
	var conn driver.Connector
	conn, err = db.GetConnector(ctx)
	if err != nil {
		return
	}

	d = sqlx.NewDb(sql.OpenDB(conn), "mysql")
	return
}
