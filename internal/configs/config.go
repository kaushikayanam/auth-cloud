package configs

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	"nononsensecode.com/auth/internal/configs/cloud/aws"
	"nononsensecode.com/auth/internal/configs/common"
	"nononsensecode.com/auth/internal/configs/local"
	"nononsensecode.com/auth/internal/pkg/domain/model/user"
	"nononsensecode.com/auth/internal/pkg/infrastructure/db"
	mysqldb "nononsensecode.com/auth/internal/pkg/infrastructure/db/mysql"
	sqllitedb "nononsensecode.com/auth/internal/pkg/infrastructure/db/sqllite"
	"nononsensecode.com/auth/internal/pkg/interfaces/openapi"
)

type Config struct {
	Server                  common.ServerConfig    `mapstructure:"server"`
	Configs                 map[string]interface{} `mapstructure:"configs"`
	isInitialized           bool
	httpMiddlewareProviders []openapi.MiddlewareProvider
}

func (cfg *Config) Init() {
	cfg.isInitialized = true

	var (
		connProviders []db.ConnectorProvider
		ok            bool
	)

	for cName, configuration := range cfg.Configs {
		switch cName {
		case "local":
			var lcfg local.LocalConfig
			if lcfg, ok = configuration.(local.LocalConfig); !ok {
				if err := mapstructure.Decode(configuration, &lcfg); err != nil {
					panic(err)
				}
				lcfg.InitDB(cfg.Server.Persistence.Vendor)
				connProviders = append(connProviders, &lcfg)
				cfg.httpMiddlewareProviders = append(cfg.httpMiddlewareProviders, &lcfg)
			}
		case "aws":
			var acfg aws.AWSConfig
			if acfg, ok = configuration.(aws.AWSConfig); !ok {
				if err := mapstructure.Decode(configuration, &acfg); err != nil {
					panic(err)
				}
				acfg.InitDB(cfg.Server.Persistence.Vendor)
				connProviders = append(connProviders, &acfg)
				// cfg.httpMiddlewareProviders = append(cfg.httpMiddlewareProviders, &acfg)
			}
		default:
			panic(fmt.Errorf("invalid configuration %s", cName))
		}
	}

	db.Init(connProviders)
}

func (c *Config) UserRepository() (r user.Repository, err error) {
	if !c.isInitialized {
		err = fmt.Errorf("application is not initialized")
		return
	}

	switch c.Server.Persistence.Vendor {
	case "mysql":
		r = mysqldb.MySQLUserRepository{}
	case "sqllite":
		r = sqllitedb.SQLiteUserRepository{}
	default:
		err = fmt.Errorf("there is no repository for vendor named %s", c.Server.Persistence.Vendor)
	}
	return
}

func (cfg *Config) GetHttpMiddlewareProviders() []openapi.MiddlewareProvider {
	return cfg.httpMiddlewareProviders
}
