package local

import (
	"context"
	"database/sql/driver"
	"sync"
)

type Connector struct {
	cId string
	r   *ClientRepository
	d   driver.Driver
	m   *sync.Mutex
}

func (c *Connector) Connect(ctx context.Context) (d driver.Conn, err error) {
	c.m.Lock()
	defer c.m.Unlock()

	var dsn string

	dsn, err = c.r.GetDsnByClientId(c.cId)
	if err != nil {
		return
	}

	d, err = c.d.Open(dsn)
	return
}

func (c *Connector) Driver() driver.Driver {
	return c.d
}
