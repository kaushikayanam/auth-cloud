package local

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Client struct {
	Id  string `db:"id"`
	Dsn string `db:"dsn"`
}

type ClientRepository struct {
	db *sqlx.DB
}

func NewClientRepository(db *sqlx.DB) *ClientRepository {
	if db == nil {
		panic("client db is nil")
	}

	return &ClientRepository{db}
}

func (c *ClientRepository) GetDsnByClientId(id string) (dsn string, err error) {
	err = c.db.Get(&dsn, "SELECT dsn FROM auth_user_client WHERE id = ?", id)
	return
}
