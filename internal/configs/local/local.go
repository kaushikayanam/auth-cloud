package local

import (
	"context"
	"database/sql/driver"
	"fmt"
	"net/http"
	"sync"

	"github.com/jmoiron/sqlx"
	"nononsensecode.com/auth/internal/configs/common"
	"nononsensecode.com/auth/internal/pkg/infrastructure/db"
	"nononsensecode.com/auth/pkg/common/server/ctxtypes"
)

type LocalConfig struct {
	ClientRepoConfig ClientRepositoryConfig `mapstructure:"clientRepo"`
	d                driver.Driver
	clientRepo       *ClientRepository
	httpMiddlewares  []func(http.Handler) http.Handler
}

func (l *LocalConfig) GetMiddlewares() []func(http.Handler) http.Handler {
	return l.httpMiddlewares
}

type ClientRepositoryConfig struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	DbName   string `mapstructure:"dbName"`
}

func (c *ClientRepositoryConfig) dsn() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", c.Username, c.Password, c.Host, c.Port, c.DbName)
}

func (l *LocalConfig) NewConnector(ctx context.Context) (d driver.Connector, err error) {
	if l.clientRepo == nil {
		err = fmt.Errorf("client repo is not initialized")
		return
	}

	var (
		clientId string
		ok       bool
	)
	if clientId, ok = ctx.Value(ctxtypes.CtxClientIdKey).(string); !ok {
		err = fmt.Errorf("there is no client id in context")
		return
	}
	d = &Connector{
		cId: clientId,
		r:   l.clientRepo,
		d:   l.d,
		m:   &sync.Mutex{},
	}
	return
}

func (l *LocalConfig) InitDB(dbName string) {
	var (
		err      error
		clientDb *sqlx.DB
	)

	clientDb, err = sqlx.Open("mysql", l.ClientRepoConfig.dsn())
	if err != nil {
		panic(err)
	}

	l.clientRepo = NewClientRepository(clientDb)

	l.d, err = common.InitDriver(dbName)
	if err != nil {
		panic(err)
	}

	l.httpMiddlewares = make([]func(http.Handler) http.Handler, 0)
	l.httpMiddlewares = append(l.httpMiddlewares, setClientId)
	l.httpMiddlewares = append(l.httpMiddlewares, setVendor)
}

func (l *LocalConfig) Provider() (pName string, p db.ConnectorProvider) {
	pName = "local"
	p = l
	return
}
