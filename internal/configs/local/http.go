package local

import (
	"context"
	"net/http"

	"nononsensecode.com/auth/pkg/common/server/ctxtypes"
)

func setClientId(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		clientId := r.Header.Get("X-Client-Id")
		if clientId == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		ctx = context.WithValue(ctx, ctxtypes.CtxClientIdKey, clientId)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

func setVendor(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		vendor := "local"
		ctx := context.WithValue(r.Context(), ctxtypes.CtxVendorKey, vendor)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
