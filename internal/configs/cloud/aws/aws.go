package aws

import (
	"context"
	"database/sql/driver"
	"fmt"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/aws/aws-secretsmanager-caching-go/secretcache"
	"nononsensecode.com/auth/internal/configs/common"
	"nononsensecode.com/auth/internal/pkg/infrastructure/db"
)

type AWSConfig struct {
	MaxCacheSize int   `mapstructure:"maxCacheSize"`
	CacheItemTTL int64 `mapstructure:"cacheItemTTL"`
	cache        *secretcache.Cache
	d            driver.Driver
}

func (a *AWSConfig) InitDB(driverName string) {
	var err error
	a.d, err = common.InitDriver(driverName)
	if err != nil {
		panic(err)
	}

	sess, err := session.NewSession(&aws.Config{})
	if err != nil {
		panic(err)
	}

	sMgr := secretsmanager.New(sess)
	cacheConfig := secretcache.CacheConfig{
		MaxCacheSize: a.MaxCacheSize,
		CacheItemTTL: a.CacheItemTTL,
	}
	a.cache, err = secretcache.New(
		func(c *secretcache.Cache) { c.CacheConfig = cacheConfig },
		func(c *secretcache.Cache) { c.Client = sMgr },
	)
	if err != nil {
		panic(err)
	}
}

func (a *AWSConfig) NewConnector(ctx context.Context) (conn driver.Connector, err error) {
	if a.cache == nil {
		err = fmt.Errorf("aws configuration is not initialized")
		return
	}

	var (
		secretName string
		ok         bool
	)
	if secretName, ok = ctx.Value("secretName").(string); !ok {
		err = fmt.Errorf("secret name for aws cannot be found")
		return
	}

	conn = &Connector{
		secretName: secretName,
		d:          a.d,
		cache:      a.cache,
		m:          &sync.Mutex{},
	}
	return
}

func (a *AWSConfig) Provider() (pName string, p db.ConnectorProvider) {
	pName = "aws"
	p = a
	return
}
