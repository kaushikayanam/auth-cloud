package cli

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"nononsensecode.com/auth/internal/configs"
	"nononsensecode.com/auth/internal/pkg/interfaces/openapi"
	"nononsensecode.com/auth/pkg/common/server"
)

var (
	cfgFile string
	cfg     *configs.Config
	authCmd = &cobra.Command{
		Use:   "auth",
		Short: "start the auth api server",
		Run:   initServer,
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	authCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "configuration file in the yaml format. Default is ./config.yaml")
}

func initConfig() {
	fmt.Println("init config called....")
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath("./")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	cfg = new(configs.Config)
	if err := viper.Unmarshal(cfg); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}

func initServer(cmd *cobra.Command, args []string) {
	fmt.Println("init server called...")
	cfg.Init()

	s, err := openapi.NewServer(cfg.UserRepository)
	if err != nil {
		panic(err)
	}

	server.RunHTTPServer(cfg.Server.Address(), func(r chi.Router) http.Handler {
		for _, mwp := range cfg.GetHttpMiddlewareProviders() {
			for _, mw := range mwp.GetMiddlewares() {
				r.Use(mw)
			}
		}
		return openapi.HandlerFromMux(s, r)
	}, cfg.Server.Cors.ChiCorsOptions(), cfg.Server.ApiPrefix)
}
